from django import forms
from .models import News, Comment


class SubmitNews(forms.ModelForm):
    class Meta:
        model = News
        fields = ('title', 'body', 'url')

    def clean(self):
        body = self.cleaned_data['body']
        url = self.cleaned_data['url']
        if body or url:
            return self.cleaned_data
        raise forms.ValidationError(
            "either one of body or url should be filled")


class CommentForm(forms.ModelForm):
    body = forms.CharField(label='', widget=forms.Textarea(
        attrs={'placeholder': 'add some comment', 'rows': 2, 'cols': 50, }))

    class Meta:
        model = Comment
        fields = ('body',)
