from django.conf import settings
from django.db import models
from django.utils import timezone
# Create your models here.


class News(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE, related_name="newsposts")
    title = models.CharField(max_length=100, blank=False)
    body = models.TextField(blank=True, null=True)
    url = models.CharField(max_length=100, blank=True, null=True)
    timestamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title


class Comment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE, related_name="comments")
    news = models.ForeignKey('News',
                             on_delete=models.CASCADE, related_name="comments")
    body = models.TextField(blank=False)
    parent = models.ForeignKey('Comment',
                               on_delete=models.CASCADE, related_name="sub_comments", null=True)
    timestamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.body
