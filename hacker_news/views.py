from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from .models import News, Comment
from .forms import SubmitNews, CommentForm
# Create your views here.


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('news')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


def news(request):
    news = News.objects.all()
    user = request.user
    print(user.username)
    return render(request, 'hacker_news/news.html', {'news': news, 'user': user})


@login_required
def comment(request, pk):
    news = get_object_or_404(News, pk=pk)
    current_user = request.user
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid:
            comment = form.save(commit=False)
            body = form.cleaned_data['body']
            comment.user = current_user
            comment.news = news
            comment.save()
            return redirect('news_comments', pk=news.pk)
    else:
        form = CommentForm()
    return render(request, 'hacker_news/comment.html', {'form': form, 'news': news})


@login_required
def reply(request, news_pk, comment_pk):
    news = get_object_or_404(News, pk=news_pk)
    parent_comment = get_object_or_404(Comment, pk=comment_pk)
    current_user = request.user
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid:
            comment = form.save(commit=False)
            body = form.cleaned_data['body']
            comment.user = current_user
            comment.news = news
            comment.parent = parent_comment
            comment.save()
            return redirect('news_comments', pk=news.pk)
    else:
        form = CommentForm()
    return render(request, 'hacker_news/reply.html', {'form': form, 'parent_comment': parent_comment})


def news_comments(request, pk):
    news = get_object_or_404(News, pk=pk)
    comments = news.comments.all()
    form = CommentForm()
    return render(request, 'hacker_news/news_comments.html', {'comments': comments, 'news': news, 'form': form})


def add_news(request):
    current_user = request.user
    if request.method == 'POST':
        form = SubmitNews(request.POST)
        if form.is_valid:
            news = form.save(commit=False)
            body = form.cleaned_data['body']
            news.user = current_user
            news.save()
            return redirect('news')
    else:
        form = SubmitNews()
    return render(request, 'hacker_news/submit.html', {'form': form})

# def comments_dict(comments):
#     comment_dic = {}
#     for comment in comments:
#         comment_dic[comment.pk] = sub_comment_dict(comment,{})


# def sub_comment_dict(comment, dict):
#     if comment is None:
#         return
#     dict['username']= comment.user.username
#     dict['timestamp']= comment.timestamp
#     dict['body']= comment.body
#     dict['childrens']= sub_comment_dict(comment.sub_comments,)
#     return
