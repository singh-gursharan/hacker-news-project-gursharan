
from django.urls import path, include
# from django.conf.urls import url
# from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', views.news, name="news"),
    # url(r'^login/$', auth_views.login,
    #     {'template_name': 'auth/login.html'}, name='login'),
    # url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    path('signup/', views.signup, name='signup'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('news/<int:pk>/comment', views.comment, name="comment"),
    path('news/add_news', views.add_news, name="add_news"),
    path('news/<int:pk>', views.news_comments, name="news_comments"),
    path('news/<int:news_pk>/<int:comment_pk>/', views.reply, name="reply"),
]
